from io import StringIO
import numpy as np
import csv
from operator import itemgetter
from time import process_time
from multiprocessing import Process
import multiprocessing
t1_start = process_time()

crossref = []
Pedigree = []
cattle_breeds = []
country_iso = []
error_values = []
# Pedigree = np.loadtxt("Pedigree_20_07.txt", dtype="str")
# print(Pedigree)
with open("Pytchon_Poligon_31_07_2023/Pedigree_test.txt", "r") as f:
    Pedigree = list(csv.reader(f, delimiter= " "))
with open("Pytchon_Poligon_31_07_2023/crossref_ALL.csv", "r") as f:
    crossref = list(csv.reader(f, delimiter= ","))
with open("Pytchon_Poligon_31_07_2023/cattle_breeds.csv", "r") as f:
    reader = csv.reader(f, delimiter= ",")
    next(reader)
    cattle_breeds = list(reader)
with open("Pytchon_Poligon_31_07_2023/country_iso.csv", "r") as f:
    reader = csv.reader(f, delimiter= ",")
    next(reader)
    country_iso = list(reader)


for num, val in enumerate(Pedigree):
    if len(val) != 3:
        print(Pedigree[num])
        error_values.append(Pedigree[num])
        del Pedigree[num]

for num, val in enumerate(cattle_breeds):
    if len(val) != 3:
        print(cattle_breeds[num])
        error_values.append(cattle_breeds[num])
        del cattle_breeds[num]

for num, val in enumerate(country_iso):
    if len(val) != 3:
        print(country_iso[num])
        error_values.append(country_iso[num])
        del Pedigree[num]

for num, val in enumerate(crossref):
    if len(val) != 2:
        print(crossref[num])
        error_values.append(crossref[num])
        del crossref[num]

def dell_dupl(tab):
    unique = []
    for row in tab:
        if row not in unique:
            unique.append(row)
    return unique

Pedigree = dell_dupl(Pedigree)

def sort_cross_Pedigree(start , stop):
    for i in range(int(start) , int(stop)):
        for j in range(0,2):
            for crossref_y in crossref:
                    if Pedigree[i][j] == crossref_y[1]:
                        Pedigree[i][j] = crossref_y[0]



cpu_count = multiprocessing.cpu_count()
num_thread_start = len(Pedigree)//cpu_count
threads = []


for n in range(cpu_count):
    t = Process(target=sort_cross_Pedigree, args=(n*num_thread_start, (n+1)*num_thread_start))
    t.start()
    threads.append(t)


for thread in threads:
    thread.join()


Pedigree = [row for row in Pedigree if row[0] != 0]



for Pedigree_y in range(len(Pedigree)):
    for Pedigree_x in range(len(Pedigree[Pedigree_y])):
        if Pedigree[Pedigree_y][Pedigree_x] !="0":
            try:
                if("RED" in Pedigree[Pedigree_y][Pedigree_x]):
                    Pedigree[Pedigree_y][Pedigree_x] = Pedigree[Pedigree_y][Pedigree_x].replace("RED", "HOL")
                if int(Pedigree[Pedigree_y][Pedigree_x][7:]) > 3000000000 and "USA" in Pedigree[Pedigree_y][Pedigree_x][::6]:
                    #print("840 1: ",Pedigree[Pedigree_y][Pedigree_x])
                    Pedigree[Pedigree_y][Pedigree_x] = Pedigree[Pedigree_y][Pedigree_x][:3] + "840" + Pedigree[Pedigree_y][Pedigree_x][6:]
                    #print("840 2: ",Pedigree[Pedigree_y][Pedigree_x])
                elif int(Pedigree[Pedigree_y][Pedigree_x][7:]) <3000000000 and "840" in Pedigree[Pedigree_y][Pedigree_x][::6]:
                    #print("USA 1: ",Pedigree[Pedigree_y][Pedigree_x])
                    Pedigree[Pedigree_y][Pedigree_x] = Pedigree[Pedigree_y][Pedigree_x][:3] + "USA" + Pedigree[Pedigree_y][Pedigree_x][6:]
                    #print("USA 2: ",Pedigree[Pedigree_y][Pedigree_x])
            except Exception as e:
                error_values.append(list([Pedigree[Pedigree_y][Pedigree_x],e]))
                continue


def clear_cattle(tab):
    unique = []
    for tab_y in tab:
        if(tab_y[1]!="" and tab_y[2]!=""):
            unique.append(tab_y)
    return unique

cattle_breeds = clear_cattle(cattle_breeds)




for i in range(len(Pedigree)):
    for j in range(len(Pedigree[i])):
        for cattle_breed in cattle_breeds:
                if cattle_breed[2] in Pedigree[i][j][:2] and cattle_breed[1] != Pedigree[i][j][:3]:
                    Pedigree[i][j] = Pedigree[i][j].replace(cattle_breed[2], cattle_breed[1])



for i in range(len(Pedigree)):
    for j in range(len(Pedigree[i])):
        for country_iso_y in country_iso:
                if country_iso_y[1] in Pedigree[i][j][4:5] and country_iso_y[2] != Pedigree[i][j][4:6]:
                    Pedigree[i][j] = Pedigree[i][j].replace(country_iso_y[1], country_iso_y[2])

#
for num ,Pedigree_y in enumerate(Pedigree):
    for Pedigree_x in Pedigree_y:
        if len(Pedigree_x) != 19 and Pedigree_x!="0" or not Pedigree_x.isalnum() and Pedigree_x!="0":
            # print(Pedigree_y)
            error_values.append(Pedigree_y)
            del Pedigree[num]
            break


proto_Pedigree= []
work_Pedigree = []
key_Pedigree = []

for Pedigree_y in range(len(Pedigree)):
    if Pedigree[Pedigree_y][1]=="0" and Pedigree[Pedigree_y][2]=="0":
        work_Pedigree.append(Pedigree[Pedigree_y])
        key_Pedigree.append(list([Pedigree[Pedigree_y][0],1]))

    else:
        work_Pedigree.append(Pedigree[Pedigree_y])
        key_Pedigree.append(list([Pedigree[Pedigree_y][0],0]))


print(len(Pedigree))
print(len(work_Pedigree))
print(len(key_Pedigree))




for key_Pedigree_y in range(1 , len(key_Pedigree)):
    for key_Pedigree_values in range(len(key_Pedigree)):
        if key_Pedigree[key_Pedigree_values][1] == key_Pedigree_y:
                for work_Pedigree_y in range(len(work_Pedigree)):
                    try:
                        if key_Pedigree[key_Pedigree_values][0] == work_Pedigree[work_Pedigree_y][1] and work_Pedigree[work_Pedigree_y][2] != "0"and [item[1] for item in key_Pedigree if item[0] == work_Pedigree[work_Pedigree_y][1]][0] != 0:
                            if [item[1] for item in key_Pedigree if item[0] == work_Pedigree[work_Pedigree_y][2]][0] !=0:
                                key_1 = [item[1] for item in key_Pedigree if item[0] == work_Pedigree[work_Pedigree_y][1]][0]
                                key_2 = [item[1] for item in key_Pedigree if item[0] == work_Pedigree[work_Pedigree_y][2]][0]
                                if key_1 > key_2:
                                    key_Pedigree[[num for num ,item in enumerate(key_Pedigree) if item[0] == work_Pedigree[work_Pedigree_y][0]][0]][1] = key_1 + 1
                                    del work_Pedigree[work_Pedigree_y]
                                elif key_1 < key_2:
                                    key_Pedigree[[num for num ,item in enumerate(key_Pedigree) if item[0] == work_Pedigree[work_Pedigree_y][0]][0]][1] = key_2 + 1
                                    del work_Pedigree[work_Pedigree_y]
                                elif key_1 == key_2:
                                    key_Pedigree[[num for num ,item in enumerate(key_Pedigree) if item[0] == work_Pedigree[work_Pedigree_y][0]][0]][1] = key_1 + 1
                                    del work_Pedigree[work_Pedigree_y]
                                break
                            else:
                                continue
                        if work_Pedigree[work_Pedigree_y][2]== "0" and work_Pedigree[work_Pedigree_y][1]!= "0" and [item[1] for item in key_Pedigree if item[0] == work_Pedigree[work_Pedigree_y][1]][0] != 0:
                            key = [item[1] for item in key_Pedigree if item[0] == work_Pedigree[work_Pedigree_y][1]][0]
                            key_Pedigree[[num for num ,item in enumerate(key_Pedigree) if item[0] == work_Pedigree[work_Pedigree_y][0]][0]][1] = key+1
                            del work_Pedigree[work_Pedigree_y]
                            break
                        if  key_Pedigree[key_Pedigree_values][0] == work_Pedigree[work_Pedigree_y][2] and work_Pedigree[work_Pedigree_y][1] != "0" and [item[1] for item in key_Pedigree if item[0] == work_Pedigree[work_Pedigree_y][2]][0] != 0:
                            if [item[1] for item in key_Pedigree if item[0] == work_Pedigree[work_Pedigree_y][1]] != 0:
                                key_1 = [item[1] for item in key_Pedigree if item[0] == work_Pedigree[work_Pedigree_y][1]][0]
                                key_2 = [item[1] for item in key_Pedigree if item[0] == work_Pedigree[work_Pedigree_y][2]][0]
                                if key_1 < key_2:
                                    key_Pedigree[[num for num ,item in enumerate(key_Pedigree) if item[0] == work_Pedigree[work_Pedigree_y][0]][0]][1] = key_2+1
                                    del Pedigree[work_Pedigree]
                                elif key_1 > key_2:
                                    key_Pedigree[[num for num ,item in enumerate(key_Pedigree) if item[0] == work_Pedigree[work_Pedigree_y][0]][0]][1] = key_1+1
                                    del work_Pedigree[work_Pedigree_y]
                                elif key_1 == key_2:
                                    key_Pedigree[[num for num ,item in enumerate(key_Pedigree) if item[0] == work_Pedigree[work_Pedigree_y][0]][0]][1] = key_2+1
                                    del work_Pedigree[work_Pedigree_y]
                                break
                            else:
                                continue
                        if work_Pedigree[work_Pedigree_y][1]== "0" and work_Pedigree[work_Pedigree_y][2]!= "0" and [item[1] for item in key_Pedigree if item[0] == work_Pedigree[work_Pedigree_y][2]][0] != 0:
                            key = [item[1] for item in key_Pedigree if item[0] == work_Pedigree[work_Pedigree_y][2]][0]
                            key_Pedigree[[num for num ,item in enumerate(key_Pedigree) if item[0] == work_Pedigree[work_Pedigree_y][0]][0]][1] = key+1
                            del work_Pedigree[work_Pedigree_y]
                            break
                    except Exception as e:
                        error_values.append(list([work_Pedigree[work_Pedigree_y],e]))

    if max(row[1] for row in key_Pedigree) == key_Pedigree_y:
        break


for n , val in enumerate(key_Pedigree):
    Pedigree[n].append(val[1])

for row in Pedigree:
    if row[3] ==0:
        error_values.append(row)

Pedigree = [row for row in Pedigree if row[3] != 0]
Pedigree = sorted(Pedigree , key = itemgetter(3))

with open('error_Pedigree.txt', 'w', encoding='UTF8', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(error_values)

with open('clear_Pedigree.txt', 'w', encoding='UTF8', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(Pedigree)


t1_stop = process_time()
print("Work time: ", t1_stop-t1_start)
